package conv

import (
	server "bitbucket.com/wshaman/tst/course/practice/gorpc/proto"
	"github.com/wshaman/stub_contacts"
)

func ContactP2B(c server.Contact) stub_contacts.Contact {
	return stub_contacts.Contact{
		ID:        uint(c.Id),
		Phone:     c.Phone,
		FirstName: c.FirstName,
		LastName:  c.LastName,
	}
}
