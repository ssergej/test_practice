package conv

import (
	server "bitbucket.com/wshaman/tst/course/practice/gorpc/proto"
	"github.com/wshaman/stub_contacts"
)

func ContactB2P(c stub_contacts.Contact) *server.Contact {
	return &server.Contact{
		Id:        int64(c.ID),
		Phone:     c.Phone,
		FirstName: c.FirstName,
		LastName:  c.LastName,
	}
}
