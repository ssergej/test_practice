package gorpc

import (
	"context"
	"log"
	"net"

	"bitbucket.com/wshaman/tst/course/practice/gorpc/conv"
	server "bitbucket.com/wshaman/tst/course/practice/gorpc/proto"
	"github.com/wshaman/stub_contacts"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type service struct {
	repo stub_contacts.YellowPages
}

func (s *service) Delete(_ context.Context, id *server.ID) (*server.DeleteReply, error) {
	if err := s.repo.Delete(uint(id.Id)); err != nil {
		return nil, err
	}
	return &server.DeleteReply{Status: "OK"}, nil
}

func (s *service) Load(_ context.Context, in *server.ID) (*server.LoadReply, error) {
	contact, err := s.repo.Load(uint(in.Id))
	if err != nil {
		return nil, err
	}
	return &server.LoadReply{People: conv.ContactB2P(contact)}, nil
}

func (s *service) Save(_ context.Context, in *server.Contact) (*server.SaveReply, error) {
	uid, err := s.repo.Save(conv.ContactP2B(*in))
	if err != nil {
		return nil, err
	}
	return &server.SaveReply{Id: int64(uid)}, nil
}

func (s *service) ListByPhone(_ context.Context, req *server.Phone) (*server.ListReply, error) {
	list, err := s.repo.FindByPhone(req.Phone)
	if err != nil {
		return nil, err
	}
	reply := &server.ListReply{
		People: make([]*server.Contact, len(list), len(list)),
	}
	for i, v := range list {
		reply.People[i] = conv.ContactB2P(v)
	}
	return reply, nil
}

func main() {

	// Стартуем наш gRPC сервер для прослушивания tcp
	tcp, err := net.Listen("tcp", ":8082")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	bookRepo, err := stub_contacts.NewYellowPages()
	if err != nil {
		panic(err)
	}
	if err = stub_contacts.Populate(bookRepo); err != nil {
		panic(err)
	}

	server.RegisterContactBookServer(s, &service{repo: bookRepo})
	reflection.Register(s)
	if err := s.Serve(tcp); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
