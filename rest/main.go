package main

import (
	"net/http"

	"bitbucket.com/wshaman/tst/course/practice/rest/handlers/contact"
	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/api/v1/contact", contact.HandleList).Methods(http.MethodGet)
	router.HandleFunc("/api/v1/contact", contact.HandleCreate).Methods(http.MethodPost)
	router.HandleFunc("/api/v1/contact/{id}", contact.HandleUpdate).Methods(http.MethodPut)
	router.HandleFunc("/api/v1/contact/{id}", contact.HandleLoad).Methods(http.MethodGet)
	http.ListenAndServe(":8081", router)
}
